(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({"D:\\Learn\\slider-ui\\src\\src\\scripts\\main.js":[function(require,module,exports){
'use strict';

// import Muc from 'views/muc'
// import View from 'views/view'
// import Component from 'components/tagihan.component'
// import Mitra from 'components/mitra.component'
// import Popup from 'components/popup.component'

$('article').readmore({
  collapsedHeight: 145,
  speed: 200
});

// Slider
// function slider(){
//   var active = $('.active').find('img').attr('src')


//   $('.big-slider').attr('src', active)


//   $('.list-thumbnail').on('click', function () {
//     var x = $(this).find('img').attr('src')

//     $('.big-slider').attr('src', x)
//     $('.list-thumbnail.active').removeClass('active')
//     $(this).addClass('active')

//   });
// }
// slider();

var active = $('.active').find('img').attr('src');
var qq = $('.active').find('h5').text();

$('.big-slider').attr('src', active);
$('.quotes').html(qq);

var slider = {
  init: function init() {
    this.dom();
    this.event();
    // this.timer();
  },
  dom: function dom() {
    this.$active = $('.active').find('img').attr('src');
    this.$list = $('.list-thumbnail');
    this.$thumb_act = $('.list-thumbnail.active');
    this.$quotes = $('.quotes');
  },
  event: function event() {
    var a = this.$list;
    var q = this.$quotes;

    a.on('click', function () {
      var x = $(this).find('img').attr('src');
      var y = $(this).find('h5');

      $('.big-slider').attr('src', x);
      $('.list-thumbnail.active').removeClass('active');
      $(this).addClass('active');

      // quotes
      q.html(y.text());
    });
  },
  timer: function timer() {
    var x = this.$thumb_act;
    var y = x.removeClass('active');
    setTimeout(function () {
      x.removeClass('active');
    }, 3000);
  }
};

slider.init();

$(document).ready(function () {
  var method = 'next';
  var i = 1;

  $('.list-thumbnail').click(function (event) {
    i = parseInt($(this).index()) + 1;
  });
  setInterval(function () {
    var x = $('.list-thumbnail.active');
    var y = $('.list-thumbnail');

    x.removeClass('active');

    if (method === 'next') {
      x.next().trigger('click');
      i++;
    } else {
      x.prev().trigger('click');
      i--;
    }

    if (i == 1) {
      method = 'next';
    } else if (i == y.length) {
      method = 'prev';
    }
  }, 5000);
});

},{}]},{},["D:\\Learn\\slider-ui\\src\\src\\scripts\\main.js"])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvc2NyaXB0cy9tYWluLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLEVBQUEsU0FBQSxFQUFBLFFBQUEsQ0FBc0I7QUFDcEIsbUJBRG9CLEdBQUE7QUFFcEIsU0FBTztBQUZhLENBQXRCOztBQUtFO0FBQ0Y7QUFDQTs7O0FBR0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxJQUFJLFNBQVMsRUFBQSxTQUFBLEVBQUEsSUFBQSxDQUFBLEtBQUEsRUFBQSxJQUFBLENBQWIsS0FBYSxDQUFiO0FBQ0EsSUFBSSxLQUFLLEVBQUEsU0FBQSxFQUFBLElBQUEsQ0FBQSxJQUFBLEVBQVQsSUFBUyxFQUFUOztBQUVBLEVBQUEsYUFBQSxFQUFBLElBQUEsQ0FBQSxLQUFBLEVBQUEsTUFBQTtBQUNBLEVBQUEsU0FBQSxFQUFBLElBQUEsQ0FBQSxFQUFBOztBQUVBLElBQUksU0FBUztBQUNYLFFBQU0sU0FBQSxJQUFBLEdBQVU7QUFDZCxTQUFBLEdBQUE7QUFDQSxTQUFBLEtBQUE7QUFDQTtBQUpTLEdBQUE7QUFNWCxPQUFLLFNBQUEsR0FBQSxHQUFVO0FBQ2IsU0FBQSxPQUFBLEdBQWUsRUFBQSxTQUFBLEVBQUEsSUFBQSxDQUFBLEtBQUEsRUFBQSxJQUFBLENBQWYsS0FBZSxDQUFmO0FBQ0EsU0FBQSxLQUFBLEdBQWEsRUFBYixpQkFBYSxDQUFiO0FBQ0EsU0FBQSxVQUFBLEdBQWtCLEVBQWxCLHdCQUFrQixDQUFsQjtBQUNBLFNBQUEsT0FBQSxHQUFlLEVBQWYsU0FBZSxDQUFmO0FBVlMsR0FBQTtBQVlYLFNBQU8sU0FBQSxLQUFBLEdBQVU7QUFDZixRQUFJLElBQUksS0FBUixLQUFBO0FBQ0EsUUFBSSxJQUFJLEtBQVIsT0FBQTs7QUFFQSxNQUFBLEVBQUEsQ0FBQSxPQUFBLEVBQWMsWUFBWTtBQUN4QixVQUFJLElBQUksRUFBQSxJQUFBLEVBQUEsSUFBQSxDQUFBLEtBQUEsRUFBQSxJQUFBLENBQVIsS0FBUSxDQUFSO0FBQ0EsVUFBSSxJQUFJLEVBQUEsSUFBQSxFQUFBLElBQUEsQ0FBUixJQUFRLENBQVI7O0FBRUEsUUFBQSxhQUFBLEVBQUEsSUFBQSxDQUFBLEtBQUEsRUFBQSxDQUFBO0FBQ0EsUUFBQSx3QkFBQSxFQUFBLFdBQUEsQ0FBQSxRQUFBO0FBQ0EsUUFBQSxJQUFBLEVBQUEsUUFBQSxDQUFBLFFBQUE7O0FBRUE7QUFDQSxRQUFBLElBQUEsQ0FBTyxFQUFQLElBQU8sRUFBUDtBQVRGLEtBQUE7QUFoQlMsR0FBQTtBQTZCWCxTQUFPLFNBQUEsS0FBQSxHQUFVO0FBQ2YsUUFBSSxJQUFJLEtBQVIsVUFBQTtBQUNBLFFBQUksSUFBSSxFQUFBLFdBQUEsQ0FBUixRQUFRLENBQVI7QUFDQSxlQUFXLFlBQVU7QUFBQyxRQUFBLFdBQUEsQ0FBQSxRQUFBO0FBQXRCLEtBQUEsRUFBQSxJQUFBO0FBQ0Q7QUFqQ1UsQ0FBYjs7QUFvQ0EsT0FBQSxJQUFBOztBQUVBLEVBQUEsUUFBQSxFQUFBLEtBQUEsQ0FBa0IsWUFBVTtBQUMxQixNQUFJLFNBQUosTUFBQTtBQUNBLE1BQUksSUFBSixDQUFBOztBQUVBLElBQUEsaUJBQUEsRUFBQSxLQUFBLENBQTJCLFVBQUEsS0FBQSxFQUFlO0FBQ3hDLFFBQUssU0FBUyxFQUFBLElBQUEsRUFBVCxLQUFTLEVBQVQsSUFBTCxDQUFBO0FBREYsR0FBQTtBQUdBLGNBQVksWUFBVTtBQUNwQixRQUFJLElBQUksRUFBUix3QkFBUSxDQUFSO0FBQ0EsUUFBSSxJQUFJLEVBQVIsaUJBQVEsQ0FBUjs7QUFFQSxNQUFBLFdBQUEsQ0FBQSxRQUFBOztBQUVBLFFBQUcsV0FBSCxNQUFBLEVBQXFCO0FBQ25CLFFBQUEsSUFBQSxHQUFBLE9BQUEsQ0FBQSxPQUFBO0FBQ0E7QUFGRixLQUFBLE1BR087QUFDTCxRQUFBLElBQUEsR0FBQSxPQUFBLENBQUEsT0FBQTtBQUNBO0FBQ0Q7O0FBRUQsUUFBSSxLQUFKLENBQUEsRUFBVztBQUNULGVBQUEsTUFBQTtBQURGLEtBQUEsTUFFTyxJQUFLLEtBQUssRUFBVixNQUFBLEVBQW9CO0FBQ3pCLGVBQUEsTUFBQTtBQUNEO0FBbEJILEdBQUEsRUFBQSxJQUFBO0FBUEYsQ0FBQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCl7ZnVuY3Rpb24gcihlLG4sdCl7ZnVuY3Rpb24gbyhpLGYpe2lmKCFuW2ldKXtpZighZVtpXSl7dmFyIGM9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZTtpZighZiYmYylyZXR1cm4gYyhpLCEwKTtpZih1KXJldHVybiB1KGksITApO3ZhciBhPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIraStcIidcIik7dGhyb3cgYS5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGF9dmFyIHA9bltpXT17ZXhwb3J0czp7fX07ZVtpXVswXS5jYWxsKHAuZXhwb3J0cyxmdW5jdGlvbihyKXt2YXIgbj1lW2ldWzFdW3JdO3JldHVybiBvKG58fHIpfSxwLHAuZXhwb3J0cyxyLGUsbix0KX1yZXR1cm4gbltpXS5leHBvcnRzfWZvcih2YXIgdT1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlLGk9MDtpPHQubGVuZ3RoO2krKylvKHRbaV0pO3JldHVybiBvfXJldHVybiByfSkoKSIsIi8vIGltcG9ydCBNdWMgZnJvbSAndmlld3MvbXVjJ1xyXG4vLyBpbXBvcnQgVmlldyBmcm9tICd2aWV3cy92aWV3J1xyXG4vLyBpbXBvcnQgQ29tcG9uZW50IGZyb20gJ2NvbXBvbmVudHMvdGFnaWhhbi5jb21wb25lbnQnXHJcbi8vIGltcG9ydCBNaXRyYSBmcm9tICdjb21wb25lbnRzL21pdHJhLmNvbXBvbmVudCdcclxuLy8gaW1wb3J0IFBvcHVwIGZyb20gJ2NvbXBvbmVudHMvcG9wdXAuY29tcG9uZW50J1xyXG5cclxuJCgnYXJ0aWNsZScpLnJlYWRtb3JlKHtcclxuICBjb2xsYXBzZWRIZWlnaHQ6IDE0NSxcclxuICBzcGVlZDogMjAwXHJcbn0pO1xyXG4gIFxyXG4gIC8vIFNsaWRlclxyXG4vLyBmdW5jdGlvbiBzbGlkZXIoKXtcclxuLy8gICB2YXIgYWN0aXZlID0gJCgnLmFjdGl2ZScpLmZpbmQoJ2ltZycpLmF0dHIoJ3NyYycpXHJcblxyXG5cclxuLy8gICAkKCcuYmlnLXNsaWRlcicpLmF0dHIoJ3NyYycsIGFjdGl2ZSlcclxuXHJcblxyXG4vLyAgICQoJy5saXN0LXRodW1ibmFpbCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcclxuLy8gICAgIHZhciB4ID0gJCh0aGlzKS5maW5kKCdpbWcnKS5hdHRyKCdzcmMnKVxyXG5cclxuLy8gICAgICQoJy5iaWctc2xpZGVyJykuYXR0cignc3JjJywgeClcclxuLy8gICAgICQoJy5saXN0LXRodW1ibmFpbC5hY3RpdmUnKS5yZW1vdmVDbGFzcygnYWN0aXZlJylcclxuLy8gICAgICQodGhpcykuYWRkQ2xhc3MoJ2FjdGl2ZScpXHJcblxyXG4vLyAgIH0pO1xyXG4vLyB9XHJcbi8vIHNsaWRlcigpO1xyXG5cclxudmFyIGFjdGl2ZSA9ICQoJy5hY3RpdmUnKS5maW5kKCdpbWcnKS5hdHRyKCdzcmMnKVxyXG52YXIgcXEgPSAkKCcuYWN0aXZlJykuZmluZCgnaDUnKS50ZXh0KClcclxuXHJcbiQoJy5iaWctc2xpZGVyJykuYXR0cignc3JjJywgYWN0aXZlKVxyXG4kKCcucXVvdGVzJykuaHRtbChxcSlcclxuXHJcbnZhciBzbGlkZXIgPSB7XHJcbiAgaW5pdDogZnVuY3Rpb24oKXtcclxuICAgIHRoaXMuZG9tKCk7XHJcbiAgICB0aGlzLmV2ZW50KCk7XHJcbiAgICAvLyB0aGlzLnRpbWVyKCk7XHJcbiAgfSxcclxuICBkb206IGZ1bmN0aW9uKCl7XHJcbiAgICB0aGlzLiRhY3RpdmUgPSAkKCcuYWN0aXZlJykuZmluZCgnaW1nJykuYXR0cignc3JjJylcclxuICAgIHRoaXMuJGxpc3QgPSAkKCcubGlzdC10aHVtYm5haWwnKVxyXG4gICAgdGhpcy4kdGh1bWJfYWN0ID0gJCgnLmxpc3QtdGh1bWJuYWlsLmFjdGl2ZScpXHJcbiAgICB0aGlzLiRxdW90ZXMgPSAkKCcucXVvdGVzJylcclxuICB9LFxyXG4gIGV2ZW50OiBmdW5jdGlvbigpe1xyXG4gICAgdmFyIGEgPSB0aGlzLiRsaXN0XHJcbiAgICB2YXIgcSA9IHRoaXMuJHF1b3Rlc1xyXG5cclxuICAgIGEub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xyXG4gICAgICB2YXIgeCA9ICQodGhpcykuZmluZCgnaW1nJykuYXR0cignc3JjJylcclxuICAgICAgdmFyIHkgPSAkKHRoaXMpLmZpbmQoJ2g1JylcclxuICBcclxuICAgICAgJCgnLmJpZy1zbGlkZXInKS5hdHRyKCdzcmMnLCB4KVxyXG4gICAgICAkKCcubGlzdC10aHVtYm5haWwuYWN0aXZlJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpXHJcbiAgICAgICQodGhpcykuYWRkQ2xhc3MoJ2FjdGl2ZScpXHJcblxyXG4gICAgICAvLyBxdW90ZXNcclxuICAgICAgcS5odG1sKHkudGV4dCgpKVxyXG4gIFxyXG4gICAgfSk7XHJcbiAgfSxcclxuICB0aW1lcjogZnVuY3Rpb24oKXtcclxuICAgIHZhciB4ID0gdGhpcy4kdGh1bWJfYWN0XHJcbiAgICB2YXIgeSA9IHgucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpXHJcbiAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7eC5yZW1vdmVDbGFzcygnYWN0aXZlJyl9LCAzMDAwKVxyXG4gIH1cclxufVxyXG5cclxuc2xpZGVyLmluaXQoKVxyXG5cclxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKXtcclxuICB2YXIgbWV0aG9kID0gJ25leHQnO1xyXG4gIHZhciBpID0gMVxyXG5cclxuICAkKCcubGlzdC10aHVtYm5haWwnKS5jbGljayhmdW5jdGlvbihldmVudCl7XHJcbiAgICBpID0gKHBhcnNlSW50KCQodGhpcykuaW5kZXgoKSkrIDEpXHJcbiAgfSlcclxuICBzZXRJbnRlcnZhbChmdW5jdGlvbigpe1xyXG4gICAgdmFyIHggPSAkKCcubGlzdC10aHVtYm5haWwuYWN0aXZlJylcclxuICAgIHZhciB5ID0gJCgnLmxpc3QtdGh1bWJuYWlsJylcclxuXHJcbiAgICB4LnJlbW92ZUNsYXNzKCdhY3RpdmUnKVxyXG5cclxuICAgIGlmKG1ldGhvZCA9PT0gJ25leHQnKXtcclxuICAgICAgeC5uZXh0KCkudHJpZ2dlcignY2xpY2snKVxyXG4gICAgICBpKys7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB4LnByZXYoKS50cmlnZ2VyKCdjbGljaycpXHJcbiAgICAgIGktLTtcclxuICAgIH1cclxuXHJcbiAgICBpZiggaSA9PSAxKXtcclxuICAgICAgbWV0aG9kID0gJ25leHQnXHJcbiAgICB9IGVsc2UgaWYgKCBpID09IHkubGVuZ3RoKSB7XHJcbiAgICAgIG1ldGhvZCA9ICdwcmV2J1xyXG4gICAgfVxyXG4gIH0sIDUwMDApXHJcbn0pXHJcblxyXG4iXX0=
