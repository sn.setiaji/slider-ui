// import Muc from 'views/muc'
// import View from 'views/view'
// import Component from 'components/tagihan.component'
// import Mitra from 'components/mitra.component'
// import Popup from 'components/popup.component'

$('article').readmore({
  collapsedHeight: 145,
  speed: 200
});
  
  // Slider
// function slider(){
//   var active = $('.active').find('img').attr('src')


//   $('.big-slider').attr('src', active)


//   $('.list-thumbnail').on('click', function () {
//     var x = $(this).find('img').attr('src')

//     $('.big-slider').attr('src', x)
//     $('.list-thumbnail.active').removeClass('active')
//     $(this).addClass('active')

//   });
// }
// slider();

var active = $('.active').find('img').attr('src')
var qq = $('.active').find('h5').text()

$('.big-slider').attr('src', active)
$('.quotes').html(qq)

var slider = {
  init: function(){
    this.dom();
    this.event();
    // this.timer();
  },
  dom: function(){
    this.$active = $('.active').find('img').attr('src')
    this.$list = $('.list-thumbnail')
    this.$thumb_act = $('.list-thumbnail.active')
    this.$quotes = $('.quotes')
  },
  event: function(){
    var a = this.$list
    var q = this.$quotes

    a.on('click', function () {
      var x = $(this).find('img').attr('src')
      var y = $(this).find('h5')
  
      $('.big-slider').attr('src', x)
      $('.list-thumbnail.active').removeClass('active')
      $(this).addClass('active')

      // quotes
      q.html(y.text())
  
    });
  },
  timer: function(){
    var x = this.$thumb_act
    var y = x.removeClass('active')
    setTimeout(function(){x.removeClass('active')}, 3000)
  }
}

slider.init()

$(document).ready(function(){
  var method = 'next';
  var i = 1

  $('.list-thumbnail').click(function(event){
    i = (parseInt($(this).index())+ 1)
  })
  setInterval(function(){
    var x = $('.list-thumbnail.active')
    var y = $('.list-thumbnail')

    x.removeClass('active')

    if(method === 'next'){
      x.next().trigger('click')
      i++;
    } else {
      x.prev().trigger('click')
      i--;
    }

    if( i == 1){
      method = 'next'
    } else if ( i == y.length) {
      method = 'prev'
    }
  }, 5000)
})

